import React from "react";
import ReactDOM from "react-dom";
import Header from "layout/Header";
import Footer from "layout/Footer";
import Content from "content/Content";

import "./index.scss";

const App = () => (
  <>
    <Header/>
    <div className="mt-10 text-3xl mx-auto max-w-6xl">
      <Content/>
      <div>Name: home</div>
      <div>Framework: react</div>
      <div>Language: JavaScript</div>
      <div>CSS: Tailwind</div>
    </div>
    <Footer/>

  </>
);
ReactDOM.render(<App />, document.getElementById("app"));
