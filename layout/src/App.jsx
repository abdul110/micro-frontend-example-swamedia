import React from "react";
import ReactDOM from "react-dom";

import "./index.scss";

const App = () => (
  <>
    <h1>Layout</h1>
  </>
);
ReactDOM.render(<App />, document.getElementById("app"));
